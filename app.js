const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const projectRoutes = require('./routes/projects');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res, next) => {
    res.render('home');
});

app.get('/about', (req, res, next) => {
    res.render('about');
})

app.get('/portfolio', (req, res, next) => {
    res.render('portfolio');
})

app.get('/contact', (req, res, next) => {
    res.render('contact');
})

app.get('/projects', (req, res, next) => {
    res.redirect('/portfolio');
})

app.use('/projects', projectRoutes.routes)

app.use((req, res, next) => {
    res.status(404).render('404', {pageTitle: 'Ooops.  That page doesn\'t exist.'});
  })


app.listen(80);