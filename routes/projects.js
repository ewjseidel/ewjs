const path = require('path');
const express = require('express');
const rootDir = require('../config/path');

const router = express.Router();

router.use('/projects', express.static(path.join(__dirname, 'public')));

router.get('/belly-art-project', (req, res, next) => {
    res.render('projects/belly-art-project');
})

router.get('/carvel', (req, res, next) => {
    res.render('projects/carvel');
})

router.get('/cobblestone', (req, res, next) => {
    res.render('projects/cobblestone');
})

router.get('/electrocore', (req, res, next) => {
    res.render('projects/electrocore');
})

router.get('/emflaza', (req, res, next) => {
    res.render('projects/emflaza');
})

router.get('/gammacare-direct', (req, res, next) => {
    res.render('projects/gammacare-direct');
})

router.get('/gammacore-uk', (req, res, next) => {
    res.render('projects/gammacore-uk');
})

router.get('/gammacore', (req, res, next) => {
    res.render('projects/gammacore');
})

router.get('/natures-own', (req, res, next) => {
    res.render('projects/natures-own');
})

router.get('/otiprio', (req, res, next) => {
    res.render('projects/otiprio');
})

router.get('/sara-blakely-foundation', (req, res, next) => {
    res.render('projects/sara-blakely-foundation');
})

router.get('/saras-notebook', (req, res, next) => {
    res.render('projects/saras-notebook');
})

router.get('/seapak', (req, res, next) => {
    res.render('projects/seapak');
})

router.get('/spanx', (req, res, next) => {
    res.render('projects/spanx');
})

router.get('/wonderbread', (req, res, next) => {
    res.render('projects/wonderbread');
})


exports.routes = router;